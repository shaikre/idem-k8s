from typing import Any
from typing import Dict


def convert_raw_deployment_to_present(hub, deployment) -> Dict[str, Any]:
    if not deployment:
        return None

    result = {"resource_id": deployment.metadata.name}
    skip_attributes = [
        "api_version",
        "kind",
        "metadata.creation_timestamp",
        "metadata.managed_fields",
        "metadata.resource_version",
        "metadata.uid",
        "spec.progress_deadline_seconds",
        "spec.revision_history_limit",
        "spec.template.spec.dns_policy",
        "spec.template.spec.scheduler_name",
        "spec.template.spec.termination_grace_period_seconds",
        "status",
    ]
    result.update(
        hub.tool.k8s.marshaller.marshal(
            k8s_object=deployment,
            skip_attributes=skip_attributes,
            skip_empty_values=True,
        )
    )
    return result
